import React from 'react';
import './App.less';
import Menu from "./Menu";
import Search from './Search';
import Language from "./Language";
import Footer from "./Footer";


const App = () => {
  return (
    <div className='App'>
      <Menu />
      <Search />
      <Language/>
      <Footer/>
    </div>
  );
};

export default App;