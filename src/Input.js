import React from 'react';
import './Input.less';
import { MdSearch } from "react-icons/md";
import { MdKeyboardVoice } from "react-icons/md";

// const Input = () => {
//   return (
//     <section className={'input'}>
//       <MdSearch className={'mdSearch'} />
//       <input type={'text'}/>
//       <MdKeyboardVoice className={'mdKeyboard'} />
//     </section>
//   );
// }

class Input extends React.Component {

  constructor(props) {
    super(props);

    // this.state = {
    //   value: ""
    // }
    //
    // this.onChangeValue = this.onChange.bind(this);

  }

  render() {
    return (
      <section className={'input'}>
        <MdSearch className={'mdSearch'} />
        <input type={'text'} onChange={this.props.onChangeFunc}/>
        <MdKeyboardVoice className={'mdKeyboard'} />
      </section>
    );
  }

  // onChange(event){
  //   this.state.value = event.target.value;
  //   console.log(this.state.value);
  // }
}

export default Input;
