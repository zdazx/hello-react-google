import React from 'react';
import './Footer.less'

const Footer = () => {
  return (
    <footer className={'footer'}>
      <p className={'HongKong'}>Hong Kong</p>
      <p className={'line'}></p>
      <ul>
        <li>Advertising</li>
        <li>Business</li>
        <li>About</li>
        <li>How Search works</li>
      </ul>
    </footer>
  );
}

export default Footer;