import React from 'react';
import './Language.less'

const Language = (props) => {
  // const buttonGoogle = 'Google Search';
  // const buttonLucky = 'I\'m Feeling Lucky';
  return (
    <section className={'language'}>
      <div className={'lang'}>
        <span>Google offered in: </span>
        <ul>
          <li><a href={''}>中文（繁體）</a></li>
          <li><a href={''}>中文（简体）</a></li>
        </ul>
      </div>
    </section>
  );
}

export default Language;