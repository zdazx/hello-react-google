import React from 'react';
import './Button.less'

// const Button = (props) => {
//   return (
//     <div className={'ButtonDiv'}>
//       <button className={'ButtonClass'}>{props.name}</button>
//     </div>
//   );
// }

class Button extends React.Component {
  constructor(props){
    super(props);
    this.onClickButton = this.onClick.bind(this);
  }
  onClick(){
    console.log("****"+this.props.inputValue);
  }

  render() {
    return (
      <div className={'ButtonDiv'}>
        <button className={'ButtonClass'} onClick={this.onClickButton}>{this.props.name}</button>
      </div>
    );
  }
}


// class Button extends React.Component{
//   constructor(props) {
//     super(props);
//
//     this.OnClickButton = this.onClick.bind(this);
//   }
//
//   onClick (){
//     console.log()
//   }
//
// }

export default Button;