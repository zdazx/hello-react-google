import React from 'react';
import './Search.less';
import './Input';
import Input from "./Input";
import Button from "./Button";

// const Search = () => {
//   return (
//     <section className='search'>
//       <header>
//         <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
//                    alt="Google Logo"/>
//       </header>
//       <Input/>
//       <Button name={'Google Search'}/>
//       <Button name={'I\'m Lucky'}/>
//     </section>
//   );
// };

class Search extends React.Component{
  constructor (props){
    super(props);
    this.state = {
      value: ""
    }
    this.getValueFromInput = this.getValueFromInput.bind(this);
  }

  render() {
    return (
      <section className='search'>
        <header>
          <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
               alt="Google Logo"/>
        </header>
        <Input onChangeFunc={this.getValueFromInput}/>
        <Button name={'Google Search'} inputValue={this.state.value}/>
        <Button name={'I\'m Lucky'}/>
      </section>
    );
  }

  getValueFromInput(event){
      this.state.value = event.target.value;
      this.setState({value: this.state.value})
      console.log("~~~"+this.state.value);
    }
}

export default Search;